import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class Main {

    private static NFA<Integer, String> nfa;

    //Sachen für effizientes Daten einlesen
    private static long startOffset;
    private static long endOffset;
    private static SeekableByteChannel sbc;
    private static final ByteBuffer buffer = ByteBuffer.allocateDirect(4096);



    /**
     * Main Methode
     * @param args
     */
    public static void main(String[] args){
        if(args.length != 2){
            System.out.println("nfa.jar <Pfad zur trans-Datei> <Pfad zur Datei mit Wörtern>");
            return;
        }

        nfa = new NFA<>(genZustaende());

        transitionenEinlesen(args[0]);

        wortAbarbeiten(args[1]);

    }



    /**
     * Erstellt die Zustände {1,2,3,...,34}
     */
    private static Set<Integer> genZustaende(){
        Set<Integer> resSet = new HashSet<>(34);
        for(int i = 1; i < 35; i++){
            resSet.add(i);
        }
        return resSet;
    }



    /**
     * Liest die Transitionen aus der Trans-Datei ein
     */
    private static void transitionenEinlesen(String pfadZurTransDatei) {
        File f = new File(pfadZurTransDatei);

        if(!f.exists() || !f.isFile()){
            System.out.println("Bitte Trans-Datei überprüfen!");
            return;
        }

        BufferedReader r = null;

        try{
            r = new BufferedReader(new FileReader(f));
            String buf;
            while((buf = r.readLine()) != null){
                String[] parts = buf.split(" ");
                nfa.addTransition(Integer.parseInt(parts[0]), parts[1], Integer.parseInt(parts[2]));
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            if(r != null){
                try {
                    r.close();
                    r = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    /**
     * Arbeitet die Wort-Datei ab
     * Die Verwendung des SeekableByteChannels habe ich mir von hier abgeguckt:
     * https://stackoverflow.com/a/30088354
     * @param pfadZurWortDatei
     */
    private static void wortAbarbeiten(String pfadZurWortDatei){
        File f = new File(pfadZurWortDatei);

        if(!f.exists() || !f.isFile()){
            System.out.println("Bitte Trans-Datei überprüfen!");
            return;
        }

        try{
            startOffset = 0;
            sbc = Files.newByteChannel(f.toPath(), StandardOpenOption.READ);
            byte[] message = null;
            while((message = leseDaten()) != null)
            {
                //ByteArray in einen String umwandeln
                String msg = new String(message);

                //Liste mit Eingabe mit den Buchstaben füllen
                List<String> eingabe = new LinkedList<>();
                Collections.addAll(eingabe, msg.split(""));

                Set<Integer> reachable = nfa.simulate(7, eingabe);
                System.out.println("Zustände für ein Wort: " + reachable);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }

    }



    private static byte[] leseDaten() throws IOException{
        endOffset = startOffset;

        boolean eol = false;
        boolean carryOver = false;
        byte[] record = null;

        while(!eol)
        {
            byte data;
            buffer.clear();
            final int bytesRead = sbc.read(buffer);

            if(bytesRead == -1)
            {
                return null;
            }

            buffer.flip();

            for(int i = 0; i < bytesRead && !eol; i++)
            {
                data = buffer.get();
                if(data == '\r' || data == '\n')
                {
                    eol = true;
                    endOffset += i;

                    if(carryOver)
                    {
                        final int messageSize = (int)(endOffset - startOffset);
                        sbc.position(startOffset);

                        final ByteBuffer tempBuffer = ByteBuffer.allocateDirect(messageSize);
                        sbc.read(tempBuffer);
                        tempBuffer.flip();

                        record = new byte[messageSize];
                        tempBuffer.get(record);
                    }
                    else
                    {
                        record = new byte[i];

                        // Need to move the buffer position back since the get moved it forward
                        buffer.position(0);
                        buffer.get(record, 0, i);
                    }

                    // Skip past the newline characters
                    // Wenn das BS ein Windows ist, müssen wir mehr überspringen
                    if(System.getProperty("os.name").toLowerCase().contains("win"))
                    {
                        startOffset = (endOffset + 2);
                    }
                    else
                    {
                        startOffset = (endOffset + 1);
                        //startOffset = endOffset;
                    }

                    // Move the file position back
                    sbc.position(startOffset);
                }
            }

            if(!eol && sbc.position() == sbc.size())
            {
                // We have hit the end of the file, just take all the bytes
                record = new byte[bytesRead];
                eol = true;
                buffer.position(0);
                buffer.get(record, 0, bytesRead);
            }
            else if(!eol)
            {
                // The EOL marker wasn't found, continue the loop
                carryOver = true;
                endOffset += bytesRead;
            }
        }

        // System.out.println(new String(record));
        return record;
    }



    private static void uebungsblattTest(){
        Set<Integer> states = new HashSet<Integer>();
        states.add(1);
        states.add(2);
        states.add(3);
        states.add(4);

        NFA<Integer, String> testNfa = new NFA<>(states);
        testNfa.addTransition(1, "rechts", 2);
        testNfa.addTransition(2, "links", 1);
        testNfa.addTransition(3, "rechts", 4);
        testNfa.addTransition(4, "links", 3);
        testNfa.addTransition(1, "runter", 3);
        testNfa.addTransition(2, "runter", 4);
        testNfa.addTransition(3, "hoch", 1);
        testNfa.addTransition(4, "hoch", 2);

        List<String> eingabe = new LinkedList<>();
        Set<Integer> reachable = new HashSet<>();

        eingabe.add("rechts");
        reachable = testNfa.simulate(1, eingabe);

        eingabe.clear();
        eingabe.add("runter");
        reachable = testNfa.simulateList(reachable, eingabe);


        eingabe.clear();
        eingabe.add("links");
        reachable = testNfa.simulateList(reachable, eingabe);


        /*
        eingabe.add("rechts");
        eingabe.add("runter");
        eingabe.add("links");
        Set<Integer> reachable = testNfa.simulate(1, eingabe);

         */

        System.out.println(reachable);
    }
}
