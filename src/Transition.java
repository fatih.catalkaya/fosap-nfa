import java.util.*;

public class Transition <S,A> {

    private HashMap<S, HashMap<A, HashSet<S>>> transitionMap;


    public Transition(){
        this.transitionMap = new HashMap<>(33);
    }


    public void add(S q, A a, S p){
        if(this.transitionMap.containsKey(q)){
            HashMap<A, HashSet<S>> beiMap = this.transitionMap.get(q);
            if(beiMap.containsKey(a)){
                HashSet<S> nachList = beiMap.get(a);
                //P hinzufügen (wenn es schon enthalten sein sollte, wird es nicht hinzugefügt)
                nachList.add(p);
            }
            else{
                //A ist noch nicht enthalten
                HashSet<S> nachList = new HashSet<>();
                nachList.add(p);
                beiMap.put(a, nachList);
            }
        }
        else{
            //Q nicht enthalten
            HashMap<A, HashSet<S>> beiMap = new HashMap<>();
            HashSet<S> nachList = new HashSet<>();

            nachList.add(p);
            beiMap.put(a, nachList);
            this.transitionMap.put(q, beiMap);
        }
    }


    public Set<S> getTransitions(Set<S> sList, A a){
        Set<S> resultSet = new HashSet<>(100);

        for(S s : sList){
            HashMap<A, HashSet<S>> beiMap = this.transitionMap.get(s);

            if(beiMap != null){
                HashSet<S> nachList = beiMap.get(a);

                if(nachList != null){
                    resultSet.addAll(nachList);
                }
            }
        }

        return resultSet;
    }
}
