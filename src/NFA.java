import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NFA<S,A> {

    Set<S> zustaende;
    Transition<S, A> transitions;
    Set<S> zuBearbeitendeZustaende;


    public NFA(Set<S> zustaende){
        this.zustaende = zustaende;
        this.transitions = new Transition<>();
    }


    public void addTransition(S q, A a, S p){
        this.transitions.add(q,a,p);
    }



    public Set<S> simulate(S q, List<A> w){
        Set<S> resultSet = new HashSet<>(10);
        resultSet.add(q);

        for(A a : w){
            resultSet = this.transitions.getTransitions(resultSet, a);
        }

        return resultSet;
    }



    public Set<S> simulateList(Set<S> qSet, List<A> w){
        Set<S> resultSet = new HashSet<>(qSet.size());
        resultSet.addAll(qSet);

        for(A a : w){
            resultSet = this.transitions.getTransitions(resultSet, a);
        }

        return resultSet;
    }
}
